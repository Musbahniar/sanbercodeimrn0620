//deklarasi library untuk input dan output 
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

//deklarasi variabel untuk daftar menu
const daftarMenu = "Silahkan Pilih Nomor Menu (1-2-3-4-5-6) \n" +
  "1. Mengubah fungsi menjadi fungsi arrow \n" +
  "2. Sederhanakan menjadi Object literal di ES6 \n" +
  "3. Destructuring \n" +
  "4. Array Spreading \n" +
  "5. Template Literals \n" +
  "6. Keluar Menu \n" +
  "Pilihan Menu? ";

readline.question(daftarMenu, (pil) => {
  pilihanMenu(pil);
})


//fungsi untuk mengambil pilihan menu
pilihanMenu = (pilihMenu) => {
  switch (pilihMenu) {
    case "1":
      goldenFunction();
      break;
    case "2":
      readline.question(`FirstName  : `, (firstName) => {
        readline.question(`LastName : `, (lastName) => {
          literalFunction(firstName, lastName);
        })
      })
      break;
    case "3":
      destructuringFunction();
      break;
    case "4":
      arraySpreadingFunction();
      break;
    case "5":
      templateLiteralsFunction();
      break;
    case "6":
      readline.close();
      break;
    default:
      console.log('Nomor Menu tidak ada');
      readline.close();
  }
}

const goldenFunction = () => {
  console.log("this is golden!!")

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

const destructuringFunction = () => {
  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const { firstName, lastName, destination, occupation, spell } = newObject;
  console.log(firstName, lastName, destination, occupation, spell)

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

const literalFunction = (firstName, lastName) => {
  const Zell = {
    firstName,
    lastName
  }

  console.log(firstName, lastName)
  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

const arraySpreadingFunction = () => {
  const west = ["Will", "Chris", "Sam", "Holly"]
  const east = ["Gill", "Brian", "Noel", "Maggie"]

  console.log(...west, ...east)

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

const templateLiteralsFunction = () => {
  const planet = "earth"
  const view = "glass"
  
  var before = `Lorem ${view} dolor sit amet,consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
  console.log(before);

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}
