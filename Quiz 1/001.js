//deklarasi library untuk input dan output 
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})


//deklarasi variabel untuk daftar menu
const daftarMenu = "Silahkan Pilih Nomor Menu (1-2-3-4) \n" +
  "1. String Terbalik \n" +
  "2. Bandingkan Angka \n" +
  "3. Palindrome  \n" +
  "4. Keluar Menu \n" +
  "Pilihan Menu? ";

readline.question(daftarMenu, (pil) => {
  pilihanMenu(pil);
})


//fungsi untuk mengambil pilihan menu
pilihanMenu = (pilihMenu) => {
  switch (pilihMenu) {
    case "1":
      readline.question(`Masukan sebuah kalimat : `, (kalimat) => {
          fnTerbalik(kalimat);
        })
      break;
    case "2":
    	readline.question(`Masukan angka pertama : `, (angka1) => {
        readline.question(`Masukan angka kedua : `, (angka2) => {
        fnMaksimum(angka1,angka2);
        })
      })
      break;
    case "3" :
    	readline.question(`Masukan sebuah kalimat : `, (kalimat) => {
          fnPalindrome(kalimat);
        })
      break;
    case "4":
      readline.close();
      break;
    default:
      console.log('Nomor Menu tidak ada');
      readline.close();
  }
}

//fungsi 1
fnTerbalik = (kalimat) => {
  var balik = '';
  var indexDepan;
  var indexBelakang;

  var counterTurun = kalimat.length;
  while (counterTurun > 0) {
    indexDepan = counterTurun-1;
    indexBelakang = indexDepan+1;
    balik += kalimat.substring(indexDepan,indexBelakang);
    counterTurun = counterTurun - 1;
  }

  console.log(balik);

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi 2
fnMaksimum = (angka1,angka2) => {
	 if(isNaN(angka1) || isNaN(angka2)){
	 		console.log('Yang Dimasukan mesti angka!!!');
	 } else if (parseInt(angka1) <0 || parseInt(angka2) <0) {
	 	  console.log('Yang Dimasukan mesti bilangan positif!!! (-1)');
	 } else if (parseInt(angka1) == parseInt(angka2)) {
	 	 console.log('Kedua Bilangan Sama Besar (-1)');
	 } else {
	 		if (parseInt(angka1) > parseInt(angka2)) {
	 			console.log('Bilangan Max :' + angka1);	
	 		} else {
	 			console.log('Bilangan Max :' + angka2);
	 		}
	 }

	  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi 3
fnPalindrome = (kalimat) => {
  var balik = '';
  var indexDepan;
  var indexBelakang;

  var counterTurun = kalimat.length;
  while (counterTurun > 0) {
    indexDepan = counterTurun-1;
    indexBelakang = indexDepan+1;
    balik += kalimat.substring(indexDepan,indexBelakang);
    counterTurun = counterTurun - 1;
  }

  if (kalimat === balik) {
  	console.log(true);	
  } else {
  	console.log(false);
  }

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}