//deklarasi library untuk input dan output 
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})


//deklarasi variabel untuk daftar menu
const daftarMenu = "Silahkan Pilih Nomor Menu (1-2-3-4) \n" +
  "1. Descending Ten \n" +
  "2. Ascending Ten \n" +
  "3. Conditional Ascending Descending  \n" +
  "4. Keluar Menu \n" +
  "Pilihan Menu? ";

readline.question(daftarMenu, (pil) => {
  pilihanMenu(pil);
})

//fungsi untuk mengambil pilihan menu
pilihanMenu = (pilihMenu) => {
  switch (pilihMenu) {
    case "1":
      readline.question(`Masukan bilangan : `, (bil) => {
          fnDescendingTen(bil);
        })
      break;
    case "2":
      readline.question(`Masukan bilangan : `, (bil) => {
          fnAscendingTen(bil);
        })
      break;
    case "3":
      readline.question(`Masukan bilangan Pertama : `, (bil) => {
        readline.question(`Masukan bilangan Kedua : `, (param) => {
          fnConditionalAscendingDescending(bil,param);
      	})
      })
      break;
    case "4":
      readline.close();
      break;
    default:
      console.log('Nomor Menu tidak ada');
      readline.close();
  }
}

//fungsi 1
fnDescendingTen = (bil) => {
	var counter = 1;
	var hasil ='';
	 if(isNaN(bil)){
	 		console.log('Yang Dimasukan mesti angka!!!');
	 } else if (parseInt(bil) < 10) {
	 	  console.log('Bilangan harus lebih besar dari 9');
	 } else {
	 	for (var i = bil; i >= 0; i--) {
	 		if (bil >= i && counter <= 10) {
	 			hasil += i + ' ';
	 		}
	 		counter = counter + 1;
	 	}

	 	console.log(hasil)
	 }


	 readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}


//fungsi 2
fnAscendingTen = (bil) => {
	var hasil = parseInt(bil)-1;
	 if(isNaN(bil)){
	 		console.log('Yang Dimasukan mesti angka!!!');
	 } else if (bil == '') {
	 	 console.log('-1');
	 } else {
	 	for (var i = 1; i <= 10; i++) {
	 			hasil = hasil + 1
	 			console.log(hasil)
	  }
	 }

	 readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi 3
fnConditionalAscendingDescending = (bil,param) => {
	 if(isNaN(bil) || isNaN(param) ){
	 		console.log('Yang Dimasukan mesti angka!!!');
	 } else {
		if(param%2==0){
    	fnDescendingTen(bil);
   	} else {
    	fnAscendingTen(bil);
   	} 	
	 }
}