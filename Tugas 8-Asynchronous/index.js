var readBooks = require('./callback.js')
var readBooksPromise = require('./promise.js')
 
//deklarasi library untuk input dan output 
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

//deklarasi variabel untuk daftar menu
const daftarMenu = "Silahkan Pilih Nomor Menu (1-2-3) \n" +
  "1. Callback Baca Buku \n" +
  "2. Promise Baca Buku \n" +
  "3. Keluar Menu \n" +
  "Pilihan Menu? ";

readline.question(daftarMenu, (pil) => {
  pilihanMenu(pil);
})


//fungsi untuk mengambil pilihan menu
pilihanMenu = (pilihMenu) => {
  switch(pilihMenu) {
    case "1" :
      readline.question(`Masukan Waktu : `, (waktu) => {
        fnCallbackBacaBuku(waktu);
      })
      break;
    case "2" :
        readline.question(`Masukan Waktu : `, (waktu) => {
          fnPromiseBacaBuku(waktu);
        })
        break;
    case "3" :
      readline.close();
      break;
    default:
      console.log('Nomor Menu tidak ada');
      readline.close();
  }
}
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

fnCallbackBacaBuku = (waktu) => {
  readBooks(waktu, books[0], function(res) {
    if (res) {
      readBooks(res, books[1], function(res1) {
        if (res1) {
          readBooks(res1, books[2], function(res2) {
            if (!res2) {
              console.log('error')
            }
          })
        }
      })
    }
  })
}

fnPromiseBacaBuku = (waktu) => {
  readBooksPromise (waktu,books[0]).then(function(res,rej){
    readBooksPromise (res,books[1]).then(function(res,rej){
      readBooksPromise (res,books[2]).then(function(res,rej){
    
      }).catch(function(error) {
        // console.log(error)
      })
    }).catch(function(error){
      // console.log(error)
    });
  }).catch (function(error){
    // console.log(error)
  });
}

