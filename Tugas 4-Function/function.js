//deklarasi library untuk input dan output 
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})


//deklarasi variabel untuk daftar menu
const daftarMenu = "Silahkan Pilih Nomor Menu (1-2-3-4) \n" +
  "1. Fungsi Teriak \n" +
  "2. Fungsi Multiply \n" +
  "3. Fungsi Introduce \n" +
  "4. Keluar Menu \n" +
  "Pilihan Menu? ";

readline.question(daftarMenu, (pil) => {
  pilihanMenu(pil);
})

//fungsi untuk mengambil pilihan menu
pilihanMenu = (pilihMenu) => {
  switch (pilihMenu) {
    case "1":
      fnTeriak();
      break;
    case "2":
      readline.question(`Masukan angka pertama : `, (angka1) => {
        readline.question(`Masukan angka kedua : `, (angka2) => {
        fnMultiply(angka1,angka2);
        })
      })
      break;
    case "3":
      readline.question(`Masukan Nama : `, (nama) => {
        readline.question(`Masukan Umur : `, (umur) => {
          readline.question(`Masukan Alamat : `, (alamat) => {
            readline.question(`Masukan Hobby : `, (hobby) => {
              fnIntroduce(nama,umur,alamat,hobby);
              })
          })
        })
      })
      break;
    case "4":
      readline.close();
      break;
    default:
      console.log('Nomor Menu tidak ada');
      readline.close();
  }
}

//fungsi Teriak
fnTeriak = () => {
  var counterNaik = 2;
  console.log('Output :');
  console.log('Halo Sanbers!');

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi Perkalian
fnMultiply = (angka1,angka2) => {
  if(!isNaN(angka1) && !isNaN(angka2)){
    var hasil = angka1 * angka2;
    console.log('Output :');
    console.log(hasil);    
  } else {
    console.log('Yang Dimasukan mesti angka!!!');
  }

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi Perkenalan
fnIntroduce = (nama = '',umur = '',alamat= '',hobby= '') => {
  console.log('Nama saya '+nama+', umur saya '+umur+' tahun, alamat saya di '+alamat+', dan saya punya hobby yaitu '+hobby);

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}