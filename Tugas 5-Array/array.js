//deklarasi library untuk input dan output 
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})


//deklarasi variabel untuk daftar menu
const daftarMenu = "Silahkan Pilih Nomor Menu (1-2-3-4-5-6-7) \n" +
  "1. Array Range \n" +
  "2. Array Range with Step \n" +
  "3. Array Sum Range \n" +
  "4. Array MUltidimensi \n" +
  "5. Array Balik Kata \n" +
  "6. Metode Array \n" +
  "7. Keluar Menu \n" +
  "Pilihan Menu? ";

readline.question(daftarMenu, (pil) => {
  pilihanMenu(pil);
})

//fungsi untuk mengambil pilihan menu
pilihanMenu = (pilihMenu) => {
  switch (pilihMenu) {
    case "1":
      readline.question(`Masukan parameter pertama : `, (startNum) => {
        readline.question(`Masukan parameter kedua : `, (finishNum) => {
        fnArrRange(startNum,finishNum);
        })
      })
      break;
    case "2":
      readline.question(`Masukan parameter pertama : `, (startNum) => {
        readline.question(`Masukan parameter kedua : `, (finishNum) => {
          readline.question(`Masukan parameter step : `, (stepNum) => {
            fnArrRangeStep(startNum,finishNum,stepNum);
            })
        })
      })
      break;
    case "3":
        readline.question(`Masukan parameter pertama : `, (startNum) => {
          readline.question(`Masukan parameter kedua : `, (finishNum) => {
            readline.question(`Masukan parameter step : `, (stepNum) => {
              fnArrSumRangeStep(startNum,finishNum,stepNum);
              })
          })
        })
        break;
    case "4" :
      dataHandling();  
    // readline.question(`Masukan Jumlah Data :`, (jml) => {
      //   dataHandling(jml);
      // })
      break; 
    case "5":
        readline.question(`Masukan sebuah kalimat : `, (kalimat) => {
          fnBalikKata(kalimat);
        })
        break;
    case "6":
        readline.question(`Masukan ID : `, (id) => {
          readline.question(`Masukan Nama : `, (nama) => {
            readline.question(`Masukan Tempat lahir : `, (tempatlahir) => {
              readline.question(`Masukan Tgl lahir (dd/mm/yyyy) : `, (tgllahir) => {
                readline.question(`Masukan hobi : `, (hobi) => {
                  fnMetodeArray(id,nama,tempatlahir,tgllahir,hobi);
                  })
                })
              })
          })
        })
        break;
    case "7":
      readline.close();
      break;
    default:
      console.log('Nomor Menu tidak ada');
      readline.close();
  }
}

//fungsi 1
fnArrRange = (startNum,finishNum) => {
  var arrIsi = [ ]
  if(!isNaN(startNum) && !isNaN(finishNum)){
    console.log('Output :');
    if(startNum < finishNum){
      for (var j = parseInt(startNum); j <= parseInt(finishNum); j++) {
        arrIsi.push(j)
      }
    } else {
      for (var j = parseInt(startNum); j > parseInt(finishNum-1); j--) {
        arrIsi.push(j)
      }
    }
    console.log(arrIsi);
  } else {
    console.log('Yang Dimasukan mesti angka!!!');
  }

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi 2
fnArrRangeStep = (startNum,finishNum,stepNum) => {
  var counter = parseInt(startNum);
  var arrIsi = [ ];

  if(!isNaN(startNum) && !isNaN(finishNum) && !isNaN(stepNum)){
    console.log('Output :');
    if(startNum < finishNum){
      while (counter <= parseInt(finishNum)) {
        arrIsi.push(counter);
        counter = counter + parseInt(stepNum);
      }
    } else {
      while (counter >= parseInt(finishNum)) {
        arrIsi.push(counter);
        counter = counter - parseInt(stepNum);
      }
    }
    console.log(arrIsi);
  } else {
    console.log('Yang Dimasukan mesti angka!!!');
  }

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi 3
fnArrSumRangeStep = (startNum,finishNum,stepNum) => {
  var counter = parseInt(startNum);
  var arrIsi = [ ];
  var cekStep;
  if (isNaN(stepNum) || stepNum === ''){
    cekStep = 1;
  } else {
    cekStep = parseInt(stepNum);
  }
  
  if(!isNaN(startNum) && !isNaN(finishNum)){
    if(startNum < finishNum){
      while (counter <= parseInt(finishNum)) {
        arrIsi.push(counter);
        counter = counter + cekStep;
      }
    } else {
      while (counter >= parseInt(finishNum)) {
        arrIsi.push(counter);
        counter = counter - cekStep;
      }
    }
    console.log('Output :' + arrIsi.reduce((a, b) => a + b, 0));
  } else {
    console.log('Yang Dimasukan mesti angka!!!');
  }

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi 4
dataHandling = () => {
  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ]; 

  for (var index = 0; index < input.length; index++) {
    console.log('Nomor ID :' + input[index][0]);
    console.log('Nama Lengkap :' +input[index][1]);
    console.log('TTL :' +input[index][2]);
    console.log('Hobi :' +input[index][3]);
    console.log('\n');
  // // recursiveAsyncReadLine();
  }

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi 5
fnBalikKata = (kalimat) => {
  var balik = '';
  var indexDepan;
  var indexBelakang;

  var counterTurun = kalimat.length;
  while (counterTurun > 0) {
    indexDepan = counterTurun-1;
    indexBelakang = indexDepan+1;
    balik += kalimat.substring(indexDepan,indexBelakang);
    counterTurun = counterTurun - 1;
  }

  console.log(balik);

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi 6
fnMetodeArray = (id,nama,tempatlahir,tgllahir,hobi)=> {
  var arrIsi = [ ];
  arrIsi.push(id,nama,tempatlahir,tgllahir,hobi);
  
  console.log(arrIsi);

  var hasilSplit = arrIsi[3].split('/');
  switch(parseInt(parseInt(hasilSplit[1]))) {
    case 1: xbulan = "Januari"; break;
    case 2: xbulan = "Februari"; break;
    case 3: xbulan = "Maret"; break;
    case 4: xbulan = "April"; break;
    case 5: xbulan = "Mei"; break;
    case 6: xbulan = "Juni"; break;
    case 7: xbulan = "Juli"; break;
    case 8: xbulan = "Agustus"; break;
    case 9: xbulan = "September"; break;
    case 10: xbulan = "Oktober"; break;
    case 11: xbulan = "November"; break;
    case 12: xbulan = "Desember"; break;
    default: xbulan = "Tidak Diketahui"; break;
  }
  console.log(xbulan);

  console.log(hasilSplit.reverse());

  console.log(arrIsi[3].split('/').join('-'));

  console.log(arrIsi[1].slice(0,15));
}


const recursiveAsyncReadLine = function () {
  readline.question(`Masukan Nama : `, (strNama) => {
    readline.question(`Masukan Alamat : `, (strAlamat) => {
      readline.question(`Masukan Hobi : `, (strHobi) => {

        readline.question(`Tambah Data (Y/n) : `, (answer) => {
          if (answer == 'n') {
            console.log(arrIsi);
            return readline.close();

          } 
        // console.log(strNama + strAlamat + strHobi);
        if (arrIsiPertama.length == 0){
          arrIsiPertama.push([strNama, strAlamat, strHobi]);
          arrIsi.push([strNama, strAlamat, strHobi]);
        } else {
          arrIsi.push([strNama, strAlamat, strHobi]);
        }
          recursiveAsyncReadLine();
        })
      })
    })
  })
};

