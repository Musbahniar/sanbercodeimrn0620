//deklarasi library untuk input dan output 
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})


//deklarasi variabel untuk daftar menu
const daftarMenu = "Silahkan Pilih Nomor Menu (1-2-3-4-5-6) \n" +
  "1. Looping While \n" +
  "2. Looping menggunakan for \n" +
  "3. Membuat Persegi Panjang \n" +
  "4. Membuat Tangga  \n" +
  "5. Membuat Papan Catur  \n" +
  "6. Keluar Menu \n" +
  "Pilihan Menu? ";

readline.question(daftarMenu, (pil) => {
  pilihanMenu(pil);
})

//fungsi untuk mengambil pilihan menu
pilihanMenu = (pilihMenu) => {
  switch (pilihMenu) {
    case "1":
      soalLoopingWhile();
      break;
    case "2":
      soalLoopingFor();
      break;
    case "3":
      readline.question(`Masukan Panjang : `, (pjg) => {
        readline.question(`Masukan Lebar : `, (lbr) => {
        soalPersegiPanjang(pjg,lbr);
        })
      })
      break;
    case "4":
        readline.question(`Masukan Tinggi : `,(tinggi) => {
          soalTangga(tinggi);
        })
        break;
    case "5":
      readline.question(`Ukuran Papan Catur : `, (ukuran) => {
        soalPapanCatur(ukuran);
      })
      break;
    case "6":
      readline.close();
      break;
    default:
      console.log('Nomor Menu tidak ada');
      readline.close();
  }
}

//fungsi Menu 1
soalLoopingWhile = () => {
  var counterNaik = 2;
  console.log('LOOPING PERTAMA');
  while (counterNaik < 22) {
    console.log(counterNaik + '-' + 'I love coding');
    counterNaik = counterNaik + 2;
  }
  console.log('LOOPING KEDUA');
  var counterTurun = 20;
  while (counterTurun > 0) {
    console.log(counterTurun + '-' + 'I love coding');
    counterTurun = counterTurun - 2;
  }

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi Menu 2
soalLoopingFor = () => {
  for (var i = 1; i < 21; i++) {
    if(i%2==0){
    console.log(i + '-' + 'Berkualitas');
    } else {
      if (i%3==0){
        console.log(i + '-' + 'I Love Coding');
      }else{
        console.log(i + '-' + 'Santai');
      }
    }
 }
 
  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi Menu 3
soalPersegiPanjang = (panjang, lebar) => {
  if (!isNaN(panjang) && !isNaN(lebar)) {
    for (var i = 1; i <= lebar; i++) {
      var persegi = '';
      for (var j = 1; j <= panjang; j++) {
        persegi += '#';
      }
      console.log(persegi);
    }
  } else {
    console.log('Yang Dimasukan mesti angka!!!');
  }

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi Menu 4
soalTangga = (tinggi) => {
  if (!isNaN(tinggi)) {
    for (var i = 1; i <= tinggi; i++) {
      var tangga = '';
      for (var j = 1; j <= i; j++) {
        tangga += '#';
      }
      console.log(tangga);
    }
  } else {
    console.log('Yang Dimasukan mesti angka!!!');
  }

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}

//fungsi Menu 5
soalPapanCatur = (ukuran) => {
  if (!isNaN(ukuran)) {
    for($n = 0; $n < ukuran; $n++) {
      catur = '';
      for($m = 0; $m < ukuran; $m++) {
        if(($n % 2 == 0 && $m % 2 == 0) || ($n % 2 == 1 && $m % 2 == 1)) {
          catur += ' ';
        } else {
          catur += '#';
        }
      }
      console.log(catur);
    } 
  } else {
    console.log('Yang Dimasukan mesti angka!!!');
  }

  readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
    pilihanMenu(pilihMenu);
  })
}