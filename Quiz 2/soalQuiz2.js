class Score {
  constructor(subject, points, email) {
      this.subject = subject
      this.points = points
      this.email = email
    }
    
    average() {
      var total = 0
      var ratarata = 0
      for (var index = 0; index < this.points.length; index++) {
        total = total + this.points[index]
      }
      ratarata = total/this.points.length;
      return 'Email:'+ this.email + ',' + 'Subjek:' + this.subject + ',' + 'Rata-rata:' + ratarata;
  }

  
}


myRata = new Score('musbahniar', [20,20,40,50], 'ruang.9campus@gmail.com');
console.log('SOAL 1----------------------------------------------')
console.log(myRata.average());
console.log('\n')

const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

let viewScores = (data,subject) => {
  var strHasil = [];

  switch(subject) {
    case 'quiz-1': 
      indexAmbil = 1;
      for (var index = 1; index < data.length; index++) {
        strHasil.push({"Email":  data[index][0], "Subject": 'nieto.value', "Points": data[index][indexAmbil]});
      }
      break;
    case 'quiz-2': 
      indexAmbil = 2;
      for (var index = 1; index < data.length; index++) {
        strHasil.push({"Email":  data[index][0], "Subject": 'nieto.value', "Points": data[index][indexAmbil]});
      }
      break;
    case 'quiz-3': 
      indexAmbil = 1;
      for (var index = 1; index < data.length; index++) {
        strHasil.push({"Email":  data[index][0], "Subject": 'nieto.value', "Points": data[index][indexAmbil]});
      }
      break;
  }
  console.log(strHasil)
}

console.log('SOAL 2----------------------------------------------')
viewScores(data,'quiz-1');
console.log('\n')

let recapScores = (data) => {
  var total = 0;
  var strpredikat;
    for (var idx = 1; idx < data.length; idx++) {
      total = (data[idx][1] + data[idx][2] + data[idx][3])/3
      if (total > 90) {
        strpredikat = 'honour';
      } else if (total > 80) {
        strpredikat = 'graduate';
      } else {
        strpredikat = 'participant';
      }
      console.log(idx,'. Email:', data[idx][0])
      console.log('Rata-rata:', total.toFixed(2))
      console.log('Predikat:', strpredikat)
      total=0
    }
}

console.log('SOAL 3----------------------------------------------')
recapScores(data);