//deklarasi library untuk input dan output 
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

//deklarasi variabel untuk daftar menu
const daftarMenu = "Silahkan Pilih Nomor Menu (1-2-3-4) \n" +
  "1. Animal Class (Release 0) \n" +
  "2. Animal Class (Release 1) \n" +
  "3. Function to Class \n" +
  "4. Keluar Menu \n" +
  "Pilihan Menu? ";

readline.question(daftarMenu, (pil) => {
  pilihanMenu(pil);
})

//fungsi untuk mengambil pilihan menu
pilihanMenu = (pilihMenu) => {
  switch(pilihMenu) {
    case "1" :
      var sheep = new Animal('shaun');
      console.log(sheep.name);
      console.log(sheep.legs);
      console.log(sheep.cold_blooded);
      readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
        pilihanMenu(pilihMenu);
      })
      break;
    case "2" :
      var kodok = new Frog('buduk');
      var sungokong = new Ape('kera sakti');
      kodok.jump();
      console.log('--------------------');
      sungokong.yell();
      
      readline.question(`Pilih Menu Lagi?`, (pilihMenu) => {
        pilihanMenu(pilihMenu);
      })
      break;
    case "3":
      var clock = new Clock({template: 'h:m:s'});
      clock.start();
      break 
    case "4" :
      readline.close();
      break;
    default:
      console.log('Nomor Menu tidak ada');
      readline.close();
  }
}

class Animal {
 constructor(name) {
   this.name = name
   this.legs = 4
   this.cold_blooded = false
 }
}

class Frog extends Animal {
  jump() {
    console.log(this.name);
    console.log(this.legs);
    console.log(this.cold_blooded);
    console.log('hop hop');
  }
}

class Ape extends Animal {
  yell() {
    console.log(this.name);
    console.log(this.legs=2);
    console.log(this.cold_blooded);
    console.log('Auooo');
  }
}

class Clock {
  constructor({ template }) {
    this.template = template;
  }

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  start() {
    this.render();
    this.timer = setInterval(()=> this.render(), 1000);
  }

  stop() {
    clearInterval(this.timer);
  }
}

