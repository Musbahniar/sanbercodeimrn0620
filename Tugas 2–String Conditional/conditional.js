//deklarasi library untuk input dan output 
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})

//deklarasi variabel untuk daftar menu
const daftarMenu = "Silahkan Pilih Nomor Menu (1-2-3) \n" +
    "1. If-else \n" +
    "2. Switch Case \n" +
    "3. Keluar Menu \n" +
    "Pilihan Menu? ";

readline.question(daftarMenu, (pil) => {
    pilihanMenu(pil);
})

//fungsi untuk mengambil pilihan menu
pilihanMenu = (pilihMenu) => {
  switch(pilihMenu) {
    case "1" :
      soalIfElse();
      break;
    case "2" :
      soalSwitchCase();
      break;
    case "3" :
      readline.close();
      break;
    default:
      console.log('Nomor Menu tidak ada');
      readline.close();
  }
}

//fungsi untuk  If Else
soalIfElse = () => {
  readline.question("Input nama = '' dan peran = '' \n" +
                  "Nama :", (nama) => {
          readline.question("Peran :", (peran) => {
            ambilNamaPeran(nama,peran);
          })
  })
}

//fungsi untuk Switch Case
soalSwitchCase = () => {
  readline.question("Input hari = '',bulan = '' dan tahun = '' \n" +
                  "hari :", (hari) => {
          readline.question("bulan :", (bulan) => {
            readline.question("tahun :", (tahun) => {
              ambilHariBulanTahun(hari,bulan,tahun);
            })
          })
  })
}

//fungsi untuk ambil dan cek nama dan peran
ambilNamaPeran = (nama,peran) => {
  if (nama == '' &&  peran == ''){
    console.log('Nama dan Peran harus diisi!');
    readline.close();
  } else if (nama == '') {
    console.log('Nama harus diisi!');
    readline.close();
  } else if (peran == '') {
    console.log('Peran harus diisi!');
    readline.close();
  } else if (peran == 'Penyihir' || peran == 'penyihir') {
    console.log("Selamat datang di Dunia Werewolf," + nama + "\n" + 
                "Halo " + peran + " " + nama + ",kamu dapat melihat siapa yang menjadi werewolf!");
    readline.close();
  } else if (peran == 'Guard' || peran == 'guard') {
    console.log("Selamat datang di Dunia Werewolf," + nama + "\n" + 
                "Halo " + peran + " " + nama + ",kamu akan membantu melindungi temanmu dari serangan werewolf.");
    readline.close();
  } else if (peran == 'Werewolf' || peran == 'werewolf') {
    console.log("Selamat datang di Dunia Werewolf," + nama + "\n" + 
                "Halo " + peran + " " + nama + ",Kamu akan memakan mangsa setiap malam!");
    readline.close();
  } else {
    console.log("Peran tidak ditemukan (Penyihir, Guard, Werewolf)");
    readline.close();
  }
}

//fungsi untukk mengambil dan merubah hari, bulan, tahun
ambilHariBulanTahun = (hari,bulan,tahun) => {
  var xbulan;
  if (hari <=0 || hari >31){
    console.log('Hari diisi angka dari 1 sd 31');
    readline.close();
  } else if (bulan <=0 || bulan >12){
    console.log('Bulan diisi angka dari 1 sd 12');
    readline.close();
  } else if (tahun < 1900 || tahun > 2200) { 
    console.log('Tahun diisi angka dari 1900 sd 2200');
    readline.close();
  } else { 
    switch(parseInt(bulan)) {
      case 1: xbulan = "Januari"; break;
      case 2: xbulan = "Februari"; break;
      case 3: xbulan = "Maret"; break;
      case 4: xbulan = "April"; break;
      case 5: xbulan = "Mei"; break;
      case 6: xbulan = "Juni"; break;
      case 7: xbulan = "Juli"; break;
      case 8: xbulan = "Agustus"; break;
      case 9: xbulan = "September"; break;
      case 10: xbulan = "Oktober"; break;
      case 11: xbulan = "November"; break;
      case 12: xbulan = "Desember"; break;
    }
    console.log("Output : ");
    console.log(hari+" "+xbulan+" "+tahun);
   readline.close();
  }

   
}